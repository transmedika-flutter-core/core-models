import 'package:json_annotation/json_annotation.dart';
part 'keep_signed_in.g.dart';

@JsonSerializable()
class KeepSignedIn{
  @JsonKey(name: 'user_name')
  final String? userName;

  @JsonKey(name: 'password')
  final String? password;

  @JsonKey(name: 'ref_type')
  final String? refType;

  KeepSignedIn(this.userName, this.password, this.refType);


  factory KeepSignedIn.fromJson(Map<String, dynamic> json) => _$KeepSignedInFromJson(json);

  Map<String, dynamic> toJson() => _$KeepSignedInToJson(this);
}