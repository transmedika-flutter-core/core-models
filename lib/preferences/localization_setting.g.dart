// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'localization_setting.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LocalizationSetting _$LocalizationSettingFromJson(Map<String, dynamic> json) =>
    LocalizationSetting(
      json['language_code'] as String,
      json['country_code'] as String?,
    );

Map<String, dynamic> _$LocalizationSettingToJson(
        LocalizationSetting instance) =>
    <String, dynamic>{
      'language_code': instance.languageCode,
      'country_code': instance.countryCode,
    };
