import 'package:core_constants/constants.dart';
import 'package:json_annotation/json_annotation.dart';
part 'localization_setting.g.dart';

@JsonSerializable()
class LocalizationSetting {
  @JsonKey(name: 'language_code')
  final String languageCode;

  @JsonKey(name: 'country_code')
  final String? countryCode;

  LocalizationSetting(this.languageCode, [this.countryCode]);

  factory LocalizationSetting.createDefault() => LocalizationSetting(Constants.defaultLocale);

  factory LocalizationSetting.fromJson(Map<String, dynamic> json) =>
      _$LocalizationSettingFromJson(json);

  Map<String, dynamic> toJson() => _$LocalizationSettingToJson(this);
}
