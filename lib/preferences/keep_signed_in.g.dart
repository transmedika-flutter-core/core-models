// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'keep_signed_in.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

KeepSignedIn _$KeepSignedInFromJson(Map<String, dynamic> json) => KeepSignedIn(
      json['user_name'] as String?,
      json['password'] as String?,
      json['ref_type'] as String?,
    );

Map<String, dynamic> _$KeepSignedInToJson(KeepSignedIn instance) =>
    <String, dynamic>{
      'user_name': instance.userName,
      'password': instance.password,
      'ref_type': instance.refType,
    };
