import 'package:core_model/entities/location.dart';
import 'package:core_model/network/response/location.dart';

extension LocationExt on LocationResponse {
  LocationEntity toEntity() => LocationEntity(
        provinceId: provinceId,
        provinceName: provinceName,
        regencyId: regencyId,
        regencyName: regencyName,
        districtId: districtId,
        districtName: districtName,
        subDistrictId: subDistrictId,
        subDistrictName: subDistrictName,
      );
}
