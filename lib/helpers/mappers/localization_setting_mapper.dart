import 'package:core_model/preferences/localization_setting.dart';
import 'package:core_model/ui/localization_setting_entity.dart';

extension LocalizationSettingExt on LocalizationSetting {
  LocalizationSettingEntity toEntity() {
    return LocalizationSettingEntity(languageCode, countryCode);
  }
}

extension LocalizationSettingEntityExt on LocalizationSettingEntity {
  LocalizationSetting toDao() {
    return LocalizationSetting(languageCode, countryCode);
  }
}
