import 'package:core_model/helpers/localization/coremodel_app_localizations.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_gen/gen_l10n/coremodel_app_localizations.dart';

extension AppLocalizationExt on BuildContext {
  CoreModelAppLocalizations getAppLocalizations() => CoreModelAppLocalizations.of(this)!;
}
