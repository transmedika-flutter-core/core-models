import 'coremodel_app_localizations.dart';

/// The translations for English (`en`).
class CoreModelAppLocalizationsEn extends CoreModelAppLocalizations {
  CoreModelAppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get empty_data => 'Empty Data';

  @override
  String get base_error_message => 'Oops, something went wrong. Please try again later or contact our support!';

  @override
  String get server_error_message => 'Sorry, an error occurred on the server. Please contact our support!';

  @override
  String get network_error_message => 'Sorry, an error occurred on network. Please check your connection or try again later.';

  @override
  String get bad_request_message => 'Sorry, an error occurred while sending the request to the server. Please contact our support!';

  @override
  String get page_not_found_message => 'Sorry, the request to the server was not found. Please contact our support!';

  @override
  String get unauthorized_error_message => 'Sorry, your authorization failed. Please re-login to continue!';
}
