import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 'coremodel_app_localizations_en.dart';
import 'coremodel_app_localizations_id.dart';

/// Callers can lookup localized strings with an instance of CoreModelAppLocalizations
/// returned by `CoreModelAppLocalizations.of(context)`.
///
/// Applications need to include `CoreModelAppLocalizations.delegate()` in their app's
/// `localizationDelegates` list, and the locales they support in the app's
/// `supportedLocales` list. For example:
///
/// ```dart
/// import 'localization/coremodel_app_localizations.dart';
///
/// return MaterialApp(
///   localizationsDelegates: CoreModelAppLocalizations.localizationsDelegates,
///   supportedLocales: CoreModelAppLocalizations.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```yaml
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # Rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the CoreModelAppLocalizations.supportedLocales
/// property.
abstract class CoreModelAppLocalizations {
  CoreModelAppLocalizations(String locale) : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static CoreModelAppLocalizations? of(BuildContext context) {
    return Localizations.of<CoreModelAppLocalizations>(context, CoreModelAppLocalizations);
  }

  static const LocalizationsDelegate<CoreModelAppLocalizations> delegate = _CoreModelAppLocalizationsDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates = <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[
    Locale('en'),
    Locale('id')
  ];

  /// No description provided for @empty_data.
  ///
  /// In en, this message translates to:
  /// **'Empty Data'**
  String get empty_data;

  /// No description provided for @base_error_message.
  ///
  /// In en, this message translates to:
  /// **'Oops, something went wrong. Please try again later or contact our support!'**
  String get base_error_message;

  /// No description provided for @server_error_message.
  ///
  /// In en, this message translates to:
  /// **'Sorry, an error occurred on the server. Please contact our support!'**
  String get server_error_message;

  /// No description provided for @network_error_message.
  ///
  /// In en, this message translates to:
  /// **'Sorry, an error occurred on network. Please check your connection or try again later.'**
  String get network_error_message;

  /// No description provided for @bad_request_message.
  ///
  /// In en, this message translates to:
  /// **'Sorry, an error occurred while sending the request to the server. Please contact our support!'**
  String get bad_request_message;

  /// No description provided for @page_not_found_message.
  ///
  /// In en, this message translates to:
  /// **'Sorry, the request to the server was not found. Please contact our support!'**
  String get page_not_found_message;

  /// No description provided for @unauthorized_error_message.
  ///
  /// In en, this message translates to:
  /// **'Sorry, your authorization failed. Please re-login to continue!'**
  String get unauthorized_error_message;
}

class _CoreModelAppLocalizationsDelegate extends LocalizationsDelegate<CoreModelAppLocalizations> {
  const _CoreModelAppLocalizationsDelegate();

  @override
  Future<CoreModelAppLocalizations> load(Locale locale) {
    return SynchronousFuture<CoreModelAppLocalizations>(lookupCoreModelAppLocalizations(locale));
  }

  @override
  bool isSupported(Locale locale) => <String>['en', 'id'].contains(locale.languageCode);

  @override
  bool shouldReload(_CoreModelAppLocalizationsDelegate old) => false;
}

CoreModelAppLocalizations lookupCoreModelAppLocalizations(Locale locale) {


  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'en': return CoreModelAppLocalizationsEn();
    case 'id': return CoreModelAppLocalizationsId();
  }

  throw FlutterError(
    'CoreModelAppLocalizations.delegate failed to load unsupported locale "$locale". This is likely '
    'an issue with the localizations generation tool. Please file an issue '
    'on GitHub with a reproducible sample app and the gen-l10n configuration '
    'that was used.'
  );
}
