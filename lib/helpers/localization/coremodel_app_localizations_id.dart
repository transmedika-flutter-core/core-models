import 'coremodel_app_localizations.dart';

/// The translations for Indonesian (`id`).
class CoreModelAppLocalizationsId extends CoreModelAppLocalizations {
  CoreModelAppLocalizationsId([String locale = 'id']) : super(locale);

  @override
  String get empty_data => 'Data tidak tersedia';

  @override
  String get base_error_message => 'Maaf terjadi kesalahan. Silahkan coba untuk beberapa saat lagi atau Silahkan hubungi kontak support kami!';

  @override
  String get server_error_message => 'Maaf terjadi kesalahan pada server. Silahkan hubungi kontak support kami!';

  @override
  String get network_error_message => 'Maaf terjadi kesalahan pada jaringan. Silahkan cek jaringan anda atau coba untuk beberapa saat lagi.';

  @override
  String get bad_request_message => 'Maaf terjadi kesalahan ketika mengirim permintaan ke server. Silahkan hubungi kontak support kami!';

  @override
  String get page_not_found_message => 'Maaf permintaan ke server tidak ditemukan. Silahkan hubungi kontak support kami!';

  @override
  String get unauthorized_error_message => 'Maaf anda tidak memiliki otoritas untuk mengakses halaman ini. Lakukan login ulang untuk melanjutkan!';
}
