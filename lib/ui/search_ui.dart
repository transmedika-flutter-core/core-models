abstract class SearchUI{
  String? getTitle();
  String? getSubTitle();
}