// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signin_param.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignInParam _$SignInParamFromJson(Map<String, dynamic> json) => SignInParam(
      json['email'] as String,
      json['password'] as String,
      json['ref_type'] as String,
    );

Map<String, dynamic> _$SignInParamToJson(SignInParam instance) =>
    <String, dynamic>{
      'email': instance.email,
      'password': instance.password,
      'ref_type': instance.refType,
    };
