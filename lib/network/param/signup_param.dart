import 'package:json_annotation/json_annotation.dart';

part 'signup_param.g.dart';

@JsonSerializable()
class SignUpParam{
  @JsonKey(name: 'full_name') 
  final String? fullName;

  @JsonKey(name: 'email')
  final String? email;
  
  @JsonKey(name: 'phone_number')
  final String? phoneNumber;
  
  @JsonKey(name: 'gender')
  final String? gender;
  
  @JsonKey(name: 'password')
  final String? password;
  
  @JsonKey(name: 'password_confirmation')
  final String? passwordConfirmation;
  
  @JsonKey(name: 'user_type')
  final String? userType;

  SignUpParam(this.fullName, this.email, this.phoneNumber, this.gender, this.password, this.passwordConfirmation, this.userType);

  factory SignUpParam.fromJson(Map<String, dynamic> json) => _$SignUpParamFromJson(json);

  Map<String, dynamic> toJson() => _$SignUpParamToJson(this);
}