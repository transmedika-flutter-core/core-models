// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'forgot_param.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ForgotParam _$ForgotParamFromJson(Map<String, dynamic> json) => ForgotParam(
      json['email'] as String,
    );

Map<String, dynamic> _$ForgotParamToJson(ForgotParam instance) =>
    <String, dynamic>{
      'email': instance.email,
    };
