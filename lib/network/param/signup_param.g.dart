// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signup_param.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignUpParam _$SignUpParamFromJson(Map<String, dynamic> json) => SignUpParam(
      json['full_name'] as String?,
      json['email'] as String?,
      json['phone_number'] as String?,
      json['gender'] as String?,
      json['password'] as String?,
      json['password_confirmation'] as String?,
      json['user_type'] as String?,
    );

Map<String, dynamic> _$SignUpParamToJson(SignUpParam instance) =>
    <String, dynamic>{
      'full_name': instance.fullName,
      'email': instance.email,
      'phone_number': instance.phoneNumber,
      'gender': instance.gender,
      'password': instance.password,
      'password_confirmation': instance.passwordConfirmation,
      'user_type': instance.userType,
    };
