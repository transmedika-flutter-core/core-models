import 'package:json_annotation/json_annotation.dart';
part 'forgot_param.g.dart';

@JsonSerializable()
class ForgotParam{
  @JsonKey(name: 'email')
  final String email;


  ForgotParam(this.email);

  factory ForgotParam.fromJson(Map<String, dynamic> json) => _$ForgotParamFromJson(json);

  Map<String, dynamic> toJson() => _$ForgotParamToJson(this);
}