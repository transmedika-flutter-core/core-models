import 'package:json_annotation/json_annotation.dart';
part 'prediction.g.dart';

@JsonSerializable()
class Prediction {
  @JsonKey(name: 'description')
  final String description;

  @JsonKey(name: 'place_id')
  final String placeId;

  @JsonKey(name: 'reference')
  final String reference;

  Prediction(this.description, this.placeId, this.reference);

  factory Prediction.fromJson(Map<String, dynamic> json) => _$PredictionFromJson(json);

  Map<String, dynamic> toJson() => _$PredictionToJson(this);
}