import 'package:core_model/network/google/place/result.dart';
import 'package:json_annotation/json_annotation.dart';

import '../map_google_base_status.dart';
part 'place_detail_response.g.dart';

@JsonSerializable()
class PlaceDetailResponse implements MapGoogleBaseStatus {

  @JsonKey(name: 'result')
  final Result result;

  @JsonKey(name: 'status')
  final String status;


  PlaceDetailResponse(this.result, this.status);

  factory PlaceDetailResponse.fromJson(Map<String, dynamic> json) => _$PlaceDetailResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PlaceDetailResponseToJson(this);

  @override
  String getStatus() {
    return this.status;
  }

}