import 'package:core_model/network/google/place/address_component.dart';
import 'package:core_model/network/google/place/geometry.dart';
import 'package:json_annotation/json_annotation.dart';
part 'result.g.dart';

@JsonSerializable()
class Result {
  @JsonKey(name: 'address_components')
  final List<AddressComponent> addressComponents;

  @JsonKey(name: 'geometry')
  final Geometry? geometry;

  Result(this.addressComponents, this.geometry);

  factory Result.fromJson(Map<String, dynamic> json) => _$ResultFromJson(json);

  Map<String, dynamic> toJson() => _$ResultToJson(this);
}