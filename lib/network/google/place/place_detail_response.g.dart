// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'place_detail_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PlaceDetailResponse _$PlaceDetailResponseFromJson(Map<String, dynamic> json) =>
    PlaceDetailResponse(
      Result.fromJson(json['result'] as Map<String, dynamic>),
      json['status'] as String,
    );

Map<String, dynamic> _$PlaceDetailResponseToJson(
        PlaceDetailResponse instance) =>
    <String, dynamic>{
      'result': instance.result,
      'status': instance.status,
    };
