import 'package:core_model/network/google/place/location.dart';
import 'package:json_annotation/json_annotation.dart';
part 'geometry.g.dart';

@JsonSerializable()
class Geometry {
  @JsonKey(name: 'location')
  final Location location;

  Geometry(this.location);

  factory Geometry.fromJson(Map<String, dynamic> json) => _$GeometryFromJson(json);

  Map<String, dynamic> toJson() => _$GeometryToJson(this);

}