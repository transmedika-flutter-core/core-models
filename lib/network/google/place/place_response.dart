import 'package:core_model/network/google/place/prediction.dart';
import 'package:json_annotation/json_annotation.dart';

import '../map_google_base_status.dart';
part 'place_response.g.dart';

@JsonSerializable()
class PlaceResponse implements MapGoogleBaseStatus {
  @JsonKey(name: 'predictions')
  final List<Prediction> predictions;

  @JsonKey(name: 'status')
  final String status;

  PlaceResponse(this.predictions, this.status);

  factory PlaceResponse.fromJson(Map<String, dynamic> json) => _$PlaceResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PlaceResponseToJson(this);

  @override
  String getStatus() {
    return this.status;
  }
}