class PagingException implements Exception {
  final String? message;

  PagingException([this.message]);

  @override
  String toString() {
    return 'Data Paging Exception: $message';
  }
}

class EmptyPagingException extends PagingException {
  EmptyPagingException([super.message]);
}
