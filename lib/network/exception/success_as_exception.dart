class SuccessAsException implements Exception {
  final String? message;

  SuccessAsException(this.message);

  @override
  String toString() {
    return 'Data Success As Execotion: $message';
  }
}