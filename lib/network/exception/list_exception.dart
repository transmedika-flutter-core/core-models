class ListException implements Exception {
  final String? message;

  ListException(this.message);

  @override
  String toString() {
    return 'Data List Exception: $message';
  }
}

class EmptyListException extends ListException {
  EmptyListException(super.message);
}