

import 'package:json_annotation/json_annotation.dart';
part 'baseresponse.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class BaseResponse<T> {
  @JsonKey(name: "success")
  final bool? status;

  @JsonKey(name: "messages")
  final String? message;

  @JsonKey(name: "data")
  final T? data;

  BaseResponse({this.status, this.message, this.data});

  factory BaseResponse.fromJson(
      Map<String, dynamic> json,
      T Function(Object? json) fromJsonT) => _$BaseResponseFromJson<T>(json, fromJsonT);

  Map<String, dynamic> toJson(Object Function(T value) toJsonT) =>
      _$BaseResponseToJson<T>(this, toJsonT);

}