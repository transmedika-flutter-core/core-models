abstract class BaseSelect{
  int getId();
  String getName();
  String code();
}