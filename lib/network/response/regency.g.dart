// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'regency.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Regency _$RegencyFromJson(Map<String, dynamic> json) => Regency(
      json['id'] as int?,
      json['regency_name'] as String?,
      json['regency_code'] as String?,
    );

Map<String, dynamic> _$RegencyToJson(Regency instance) => <String, dynamic>{
      'id': instance.id,
      'regency_name': instance.regencyName,
      'regency_code': instance.regencyCode,
    };
