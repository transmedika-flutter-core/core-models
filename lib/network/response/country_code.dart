
import 'package:core_model/network/base/base_select.dart';
import 'package:json_annotation/json_annotation.dart';
part 'country_code.g.dart';

@JsonSerializable()
class CountryCode implements BaseSelect{
  @JsonKey(name: 'id')
  final int id;

  @JsonKey(name: 'name')
  final String? name;

  @JsonKey(name: 'dial_code')
  final String? dialCode;

  @JsonKey(name: 'code')
  final String? countryCode;

  CountryCode(this.name, this.dialCode, this.countryCode, this.id);

  factory CountryCode.fromJson(Map<String, dynamic> json) => _$CountryCodeFromJson(json);

  Map<String, dynamic> toJson() => _$CountryCodeToJson(this);

  @override
  int getId() {
    return this.id;
  }

  @override
  String getName() {
    return this.name ?? '-';
  }

  @override
  String code() {
    return this.countryCode ?? '-';
  }

  @override
  String toString() {
    return this.name ?? '-';
  }
}