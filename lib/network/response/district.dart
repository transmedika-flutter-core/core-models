import 'package:core_model/network/base/base_select.dart';
import 'package:json_annotation/json_annotation.dart';

part 'district.g.dart';

@JsonSerializable()
class District implements BaseSelect{
  @JsonKey(name: 'id')
  final int? id;
  @JsonKey(name: 'district_name')
  final String? districtName;
  @JsonKey(name: 'district_code')
  final String? districtCode;

  District(this.id, this.districtName, this.districtCode);

  factory District.fromJson(Map<String, dynamic> json) => _$DistrictFromJson(json);

  Map<String, dynamic> toJson() => _$DistrictToJson(this);

  @override
  int getId() {
    return id!=null ? id! : -1;
  }

  @override
  String getName() {
    return districtName !=null ? districtName! : "";
  }

  @override
  String toString() {
    return districtName !=null ? districtName! : "";
  }

  @override
  String code() {
    return id!=null ? id.toString() : "-1";
  }
}