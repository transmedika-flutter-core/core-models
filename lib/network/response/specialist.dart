
import 'package:core_model/network/base/base_select.dart';
import 'package:json_annotation/json_annotation.dart';
part 'specialist.g.dart';

@JsonSerializable()
class Specialist implements BaseSelect{
  @JsonKey(name: 'id')
  final int? id;
  @JsonKey(name: 'name')
  final String? name;
  @JsonKey(name: 'slug')
  final String? slug;
  @JsonKey(name: 'description')
  final String? description;
  @JsonKey(name: 'image')
  final String? image;

  Specialist(this.id, this.name, this.slug, this.description, this.image);

  factory Specialist.fromJson(Map<String, dynamic> json) => _$SpecialistFromJson(json);

  Map<String, dynamic> toJson() => _$SpecialistToJson(this);

  @override
  int getId() {
    return id!=null ? id! : -1;
  }

  @override
  String getName() {
    return name!=null ? name! : "";
  }

  @override
  String toString() {
    return name!=null ? name! : "";
  }

  @override
  String code() {
    return slug!=null ? slug! : "";
  }
}