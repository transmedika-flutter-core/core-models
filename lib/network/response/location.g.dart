// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'location.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LocationResponse _$LocationResponseFromJson(Map<String, dynamic> json) =>
    LocationResponse(
      provinceId: json['province_id'] as int,
      provinceName: json['province_name'] as String,
      regencyId: json['regency_id'] as int,
      regencyName: json['regency_name'] as String,
      districtId: json['district_id'] as int,
      districtName: json['district_name'] as String,
      subDistrictId: json['sub_district_id'] as int,
      subDistrictName: json['sub_district_name'] as String,
    );

Map<String, dynamic> _$LocationResponseToJson(LocationResponse instance) =>
    <String, dynamic>{
      'province_id': instance.provinceId,
      'province_name': instance.provinceName,
      'regency_id': instance.regencyId,
      'regency_name': instance.regencyName,
      'district_id': instance.districtId,
      'district_name': instance.districtName,
      'sub_district_id': instance.subDistrictId,
      'sub_district_name': instance.subDistrictName,
    };
