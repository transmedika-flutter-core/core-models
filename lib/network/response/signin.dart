import 'package:json_annotation/json_annotation.dart';

import '../base/baseresponse.dart';
part 'signin.g.dart';

@JsonSerializable()
class SignIn {
  @JsonKey(name: 'token')
  final String? token;

  @JsonKey(name: 'X-AUTH-TOKEN')
  final String? xAuthtoken;

  @JsonKey(name: 'AUTH-TOKEN')
  final String? authtoken;

  @JsonKey(name: 'NIK')
  final String? nik;

  @JsonKey(name: 'uuid')
  final String? uuid;

  @JsonKey(name: 'full_name')
  final String? fullName;

  @JsonKey(name: 'email')
  final String? email;

  @JsonKey(name: 'phone_number')
  final String? phoneNumber;

  @JsonKey(name: 'gender')
  final String? gender;

  @JsonKey(name: 'profile_picture')
  final String? profilePicture;

  SignIn(this.token, this.fullName, this.email, this.phoneNumber, this.uuid, this.gender, this.profilePicture, this.xAuthtoken, this.nik, this.authtoken);

  factory SignIn.fromJson(Map<String, dynamic> json) => _$SignInFromJson(json);

  Map<String, dynamic> toJson() => _$SignInToJson(this);

  @override
  String toString() {
    return 'SignIn{token: $token, fullName: $fullName, email: $email, phoneNumber: $phoneNumber}';
  }

  static SignIn dummy(){
    return SignIn('Xksjhd77td', 'Widiyanto', 'widi@gmail.com', '087822204188',
        null, null, null, null, null, null);
  }

  static BaseResponse<SignIn> get dataDummy => BaseResponse(
      data: dummy()
  );
}