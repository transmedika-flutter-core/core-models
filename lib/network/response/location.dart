import 'package:json_annotation/json_annotation.dart';

part 'location.g.dart';

@JsonSerializable()
class LocationResponse {
  @JsonKey(name: "province_id")
  final int provinceId;
  @JsonKey(name: "province_name")
  final String provinceName;
  @JsonKey(name: "regency_id")
  final int regencyId;
  @JsonKey(name: "regency_name")
  final String regencyName;
  @JsonKey(name: "district_id")
  final int districtId;
  @JsonKey(name: "district_name")
  final String districtName;
  @JsonKey(name: "sub_district_id")
  final int subDistrictId;
  @JsonKey(name: "sub_district_name")
  final String subDistrictName;

  LocationResponse({
    required this.provinceId,
    required this.provinceName,
    required this.regencyId,
    required this.regencyName,
    required this.districtId,
    required this.districtName,
    required this.subDistrictId,
    required this.subDistrictName,
  });

  factory LocationResponse.fromJson(Map<String, dynamic> json) =>
      _$LocationResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LocationResponseToJson(this);
}
