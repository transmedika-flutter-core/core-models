// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'province.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Province _$ProvinceFromJson(Map<String, dynamic> json) => Province(
      json['id'] as int?,
      json['province_name'] as String?,
      json['province_code'] as String?,
      json['province_initial'] as String?,
      json['image'] as String?,
    );

Map<String, dynamic> _$ProvinceToJson(Province instance) => <String, dynamic>{
      'id': instance.id,
      'province_name': instance.provinceName,
      'province_code': instance.provinceCode,
      'province_initial': instance.provinceInitial,
      'image': instance.image,
    };
