// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Profile<T> _$ProfileFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    Profile<T>(
      json['id'] as int?,
      json['uuid'] as String,
      json['full_name'] as String,
      json['email'] as String,
      json['phone_number'] as String?,
      json['gender'] as String?,
      json['profile_picture'] as String?,
      json['status'] as String?,
      json['email_verified_at'] as String?,
      json['ref_id'] as int?,
      _$nullableGenericFromJson(json['ref'], fromJsonT),
    );

Map<String, dynamic> _$ProfileToJson<T>(
  Profile<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'id': instance.id,
      'uuid': instance.uuid,
      'full_name': instance.fullName,
      'email': instance.email,
      'phone_number': instance.phoneNumber,
      'gender': instance.gender,
      'profile_picture': instance.profilePicture,
      'status': instance.status,
      'email_verified_at': instance.emailVerifiedAt,
      'ref_id': instance.refId,
      'ref': _$nullableGenericToJson(instance.ref, toJsonT),
    };

T? _$nullableGenericFromJson<T>(
  Object? input,
  T Function(Object? json) fromJson,
) =>
    input == null ? null : fromJson(input);

Object? _$nullableGenericToJson<T>(
  T? input,
  Object? Function(T value) toJson,
) =>
    input == null ? null : toJson(input);
