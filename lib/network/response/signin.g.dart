// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signin.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignIn _$SignInFromJson(Map<String, dynamic> json) => SignIn(
      json['token'] as String?,
      json['full_name'] as String?,
      json['email'] as String?,
      json['phone_number'] as String?,
      json['uuid'] as String?,
      json['gender'] as String?,
      json['profile_picture'] as String?,
      json['X-AUTH-TOKEN'] as String?,
      json['NIK'] as String?,
      json['AUTH-TOKEN'] as String?,
    );

Map<String, dynamic> _$SignInToJson(SignIn instance) => <String, dynamic>{
      'token': instance.token,
      'X-AUTH-TOKEN': instance.xAuthtoken,
      'AUTH-TOKEN': instance.authtoken,
      'NIK': instance.nik,
      'uuid': instance.uuid,
      'full_name': instance.fullName,
      'email': instance.email,
      'phone_number': instance.phoneNumber,
      'gender': instance.gender,
      'profile_picture': instance.profilePicture,
    };
