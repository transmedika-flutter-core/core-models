import 'package:core_model/network/base/base_select.dart';
import 'package:json_annotation/json_annotation.dart';
part 'province.g.dart';

@JsonSerializable()
class Province implements BaseSelect{
  @JsonKey(name: 'id')
  final int? id;

  @JsonKey(name: 'province_name')
  final String? provinceName;
  @JsonKey(name: 'province_code')
  final String? provinceCode;
  @JsonKey(name: 'province_initial')
  final String? provinceInitial;
  @JsonKey(name: 'image')
  final String? image;

  Province(this.id, this.provinceName, this.provinceCode, this.provinceInitial, this.image);

  factory Province.fromJson(Map<String, dynamic> json) => _$ProvinceFromJson(json);

  Map<String, dynamic> toJson() => _$ProvinceToJson(this);

  @override
  int getId() {
    return id!=null ? id! : -1;
  }

  @override
  String getName() {
    return provinceName!=null ? provinceName! : "";
  }

  @override
  String toString() {
    return provinceName!=null ? provinceName! : "";
  }

  @override
  String code() {
    return id!=null ? id.toString() : "-1";
  }
}