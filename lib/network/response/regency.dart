import 'package:core_model/network/base/base_select.dart';
import 'package:json_annotation/json_annotation.dart';

part 'regency.g.dart';

@JsonSerializable()
class Regency implements BaseSelect{
  @JsonKey(name: 'id')
  final int? id;
  @JsonKey(name: 'regency_name')
  final String? regencyName;
  @JsonKey(name: 'regency_code')
  final String? regencyCode;

  Regency(this.id, this.regencyName, this.regencyCode);

  factory Regency.fromJson(Map<String, dynamic> json) => _$RegencyFromJson(json);

  Map<String, dynamic> toJson() => _$RegencyToJson(this);

  @override
  int getId() {
    return id!=null ? id! : -1;
  }

  @override
  String getName() {
    return regencyName!=null ? regencyName! : "";
  }

  @override
  String toString() {
    return regencyName!=null ? regencyName! : "";
  }

  @override
  String code() {
    return id!=null ? id.toString() : "-1";
  }
}