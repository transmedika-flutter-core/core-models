import 'package:core_model/network/base/base_select.dart';
import 'package:json_annotation/json_annotation.dart';

part 'sub_district.g.dart';

@JsonSerializable()
class SubDistrict implements BaseSelect{
  @JsonKey(name: 'id')
  final int? id;
  @JsonKey(name: 'sub_district_name')
  final String? subDistrictName;
  @JsonKey(name: 'district_code')
  final String? subDistrictCode;

  SubDistrict(this.id, this.subDistrictName, this.subDistrictCode);

  factory SubDistrict.fromJson(Map<String, dynamic> json) => _$SubDistrictFromJson(json);

  Map<String, dynamic> toJson() => _$SubDistrictToJson(this);

  @override
  int getId() {
    return id!=null ? id! : -1;
  }

  @override
  String getName() {
    return subDistrictName !=null ? subDistrictName! : "";
  }

  @override
  String toString() {
    return subDistrictName !=null ? subDistrictName! : "";
  }

  @override
  String code() {
    return id!=null ? id.toString() : "-1";
  }
}