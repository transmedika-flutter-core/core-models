import 'package:json_annotation/json_annotation.dart';

part 'profile.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class Profile<T>{
  @JsonKey(name: 'id')
  final int? id;

  @JsonKey(name: 'uuid')
  final String uuid;

  @JsonKey(name: 'full_name')
  final String fullName;

  @JsonKey(name: 'email')
  final String email;

  @JsonKey(name: 'phone_number')
  final String? phoneNumber;

  @JsonKey(name: 'gender')
  final String? gender;

  @JsonKey(name: 'profile_picture')
  final String? profilePicture;

  @JsonKey(name: 'status')
  final String? status;

  @JsonKey(name: 'email_verified_at')
  final String? emailVerifiedAt;

  @JsonKey(name: 'ref_id')
  final int? refId;

  @JsonKey(name: "ref")
  final T? ref;

  Profile(this.id, this.uuid, this.fullName, this.email, this.phoneNumber, this.gender, this.profilePicture, this.status, this.emailVerifiedAt, this.refId, this.ref);

  factory Profile.fromJson(
      Map<String, dynamic> json,
      T Function(Object? json) fromJsonT) => _$ProfileFromJson<T>(json, fromJsonT);

  Map<String, dynamic> toJson(Object Function(T value) toJsonT) =>
      _$ProfileToJson<T>(this, toJsonT);
}