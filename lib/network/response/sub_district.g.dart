// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sub_district.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubDistrict _$SubDistrictFromJson(Map<String, dynamic> json) => SubDistrict(
      json['id'] as int?,
      json['sub_district_name'] as String?,
      json['district_code'] as String?,
    );

Map<String, dynamic> _$SubDistrictToJson(SubDistrict instance) =>
    <String, dynamic>{
      'id': instance.id,
      'sub_district_name': instance.subDistrictName,
      'district_code': instance.subDistrictCode,
    };
