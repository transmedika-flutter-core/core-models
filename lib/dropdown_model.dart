import 'package:core_model/network/base/base_select.dart';

class DropDownModel implements BaseSelect{
  final int id;
  final String name;
  final String slug;

  DropDownModel(this.id, this.name, this.slug);

  @override
  int getId() => id;

  @override
  String getName() => name;

  @override
  String code() => slug;

  @override
  String toString() => name;

}