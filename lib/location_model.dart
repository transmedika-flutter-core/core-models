class LocationModel{
  late String locationName;
  final double locationLat;
  final double locationLon;

  final String? administrativeAreaLevel1;
  final String? administrativeAreaLevel2;
  final String? administrativeAreaLevel3;
  final String? administrativeAreaLevel4;

  LocationModel(this.locationName, this.locationLat, this.locationLon,{
    this.administrativeAreaLevel1,
    this.administrativeAreaLevel2,
    this.administrativeAreaLevel3,
    this.administrativeAreaLevel4,
  });

  factory LocationModel.locationModel(
      String locationName,
      double locationLat,
      double locationLon,
      String? administrativeAreaLevel1,
      String? administrativeAreaLevel2,
      String? administrativeAreaLevel3,
      String? administrativeAreaLevel4,
      ) =>
      LocationModel(
        locationName,
        locationLat,
        locationLon,
        administrativeAreaLevel1: administrativeAreaLevel1,
        administrativeAreaLevel2: administrativeAreaLevel2,
        administrativeAreaLevel3: administrativeAreaLevel3,
        administrativeAreaLevel4: administrativeAreaLevel4

      );
}