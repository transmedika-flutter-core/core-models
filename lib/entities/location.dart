class LocationEntity {

  LocationEntity({
    required this.provinceId,
    required this.provinceName,
    required this.regencyId,
    required this.regencyName,
    required this.districtId,
    required this.districtName,
    required this.subDistrictId,
    required this.subDistrictName,
  });
  final int provinceId;
  final String provinceName;
  final int regencyId;
  final String regencyName;
  final int districtId;
  final String districtName;
  final int subDistrictId;
  final String subDistrictName;
}
